package aws_ec2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import aws_ec2.tasks.Homework09;
import aws_ec2.utils.EC2Manager;
import aws_ec2.utils.Key_GroupManager;
import aws_ec2.utils.MyInstanceState;
	
public class MainH09 {
	
   	private static String keypairName = "keyPairTesting";
   	private static String securityGroupName = "securityGroupTesting";
    	
	public static void main(String[] args) {
		
		Key_GroupManager.deleteKeyPair(keypairName);
		Key_GroupManager.createKeyPair(keypairName);
		String secgrID = Key_GroupManager.createSecurityGroup(securityGroupName, 7000, 17000);
		System.out.println("secgrID: " + secgrID);
		
		EC2Manager ec2_0 = new EC2Manager();
		EC2Manager ec2_1 = new EC2Manager();
		EC2Manager ec2_2 = new EC2Manager();
		EC2Manager ec2_3 = new EC2Manager();
		EC2Manager ec2_4 = new EC2Manager();
		EC2Manager ec2_5 = new EC2Manager();
		EC2Manager ec2_6 = new EC2Manager();
		EC2Manager ec2_7 = new EC2Manager();
		EC2Manager ec2_8 = new EC2Manager();
		EC2Manager ec2_9 = new EC2Manager();
		EC2Manager ec2_10 = new EC2Manager();
		EC2Manager ec2_11 = new EC2Manager();
		HashMap<EC2Manager, String> ec2s_ids = new HashMap<EC2Manager, String>();
		ec2s_ids.put(ec2_0, null);
		ec2s_ids.put(ec2_1, null);
		ec2s_ids.put(ec2_2, null);
		ec2s_ids.put(ec2_3, null);
		ec2s_ids.put(ec2_4, null);
		ec2s_ids.put(ec2_5, null);
		ec2s_ids.put(ec2_6, null);
		ec2s_ids.put(ec2_7, null);
		ec2s_ids.put(ec2_8, null);
		ec2s_ids.put(ec2_9, null);
		ec2s_ids.put(ec2_10, null);
		ec2s_ids.put(ec2_11, null);
		
       	runInitThreads(secgrID, ec2s_ids);
       	
       	printCreateCluster(ec2s_ids);
		
//       	runTerminateThreads(ec2s_ids);
//       	
//		Key_GroupManager.deleteKeyPair(keypairName);
//		Key_GroupManager.deleteKeyPairPemFile(keypairName);
//		Key_GroupManager.deleteSecurityGroup(secgrID);
//
//		System.out.println("Test terminated");
		
	}

	private static void printCreateCluster(HashMap<EC2Manager, String> ec2s_ids) {
		System.out.print("redis-cli --cluster create ");
       	for (EC2Manager entry : ec2s_ids.keySet()) {
       		System.out.print(entry.getPublicIP() + ":7000 ");
       	}
       	System.out.print(" --cluster-replicas 1");
	}

	private static void runTerminateThreads(HashMap<EC2Manager, String> ec2s_ids) {
		List<Thread> threadsTerminate = new ArrayList<Thread>();
       	for (Map.Entry<EC2Manager, String> entry : ec2s_ids.entrySet()) {
    		threadsTerminate.add(new Thread() {
 			   public void run() { 
 				  entry.getKey().terminateInstance(entry.getValue());
 				    entry.getKey().waitForInstanceStatus(MyInstanceState.terminated);
 			   }
 			});
       	}
//      start threads
		for (Thread t : threadsTerminate) {
			t.start();
		}

//      waits for threads to finish
		for (Thread t : threadsTerminate) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private static void runInitThreads(String secgrID, HashMap<EC2Manager, String> ec2s_ids) {
		
		List<Thread> threadsInits = new ArrayList<Thread>();
       	for (Map.Entry<EC2Manager, String> entry : ec2s_ids.entrySet()) {
    		threadsInits.add(new Thread() {
 			   public void run() { 
 				    String instanceID = Homework09.initRedisEC2Instance(entry.getKey(), keypairName, secgrID);
 				    ec2s_ids.put(entry.getKey(), instanceID);
 			   }
 			});
       	}
//      start threads
		for (Thread t : threadsInits) {
			t.start();
		}

//      waits for threads to finish
		for (Thread t : threadsInits) {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
