package aws_ec2.tasks;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.util.EC2MetadataUtils;

import aws_ec2.utils.EC2Manager;
import aws_ec2.utils.MyInstanceState;
import aws_ec2.utils.SSHConnection;

public class Homework08 {
	
   	private static String keypairName = "keyPairTesting";
   	private static String securityGroupName = "securityGroupTesting";
   	private static String inputVM = "t2.micro";
   	private static String keypairFilePath = "src/main/resources/keyPairTesting.pem";
   	private static String publicIP;
   	private static String privateIP;

	public static void createEC2() {
		
        EC2Manager ec2 = new EC2Manager();

        ec2.deleteKeyPair(keypairName);
        ec2.createKeyPair(keypairName);
                
        String secgrID = ec2.createSecurityGroup(securityGroupName);
        System.out.println("secgrID: "+secgrID);

        Instance instance = ec2.requestRunInstance( keypairName, secgrID, inputVM); 
        ec2.setInstanceTag(instance);
        ec2.startInstance( instance);
        
        ec2.waitForInstanceStatus(MyInstanceState.running);
        
        publicIP = ec2.getPublicIP();
        
        SSHConnection con0 = waitForSession(ec2);
        con0.sshCommand("echo ciao");
        String str = getUserData();
        con0.sshCommand(str);
//      solution to start redis-server and terminate java program using screen --d -m
        con0.sshCommand("screen -S session -d -m redis-server\n");
      	con0.sshCommand("redis-cli ping\n");
        con0.sshCommand("redis-cli -v");
//        con0.sshCommand("echo ciao");
        con0.disconnetSession();
        
//        ec2.terminateInstance(instance);
//        ec2.waitForInstanceStatus(MyInstanceState.terminated);
//
//        ec2.deleteKeyPair(keypairName); 
//        ec2.deleteKeyPairPemFile(keypairName);
//        ec2.deleteSecurityGroup(secgrID, instance);
        
        System.out.println("Test terminated");
	}
	
	private static SSHConnection waitForSession(EC2Manager ec2) {
		SSHConnection connection = new SSHConnection(ec2.getPublicIP(), keypairFilePath);
        System.out.println("trying to get connected via ssh");
        while(connection.getSession() == null) {
        	connection.connectSession();
        	try {
        		Thread.sleep(1000);
				System.out.print(".");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		return connection;
		
	}
	
	private static String getUserDataNormalNode() {
		String userData = "";
		userData += "#!/bin/bash\n";
		userData += "echo \"#!/bin/bash\nyum -y update\nsudo yum -y install gcc make\ncd /usr/local/src \nsudo wget http://download.redis.io/redis-stable.tar.gz\nsudo tar xvzf redis-stable.tar.gz\nsudo rm -f redis-stable.tar.gz\ncd redis-stable\nsudo make distclean\nsudo make\nsudo yum install -y tcl\nsudo cp src/redis-server /usr/local/bin/\nsudo cp src/redis-cli /usr/local/bin/\nsudo cp src/redis-benchmark /usr/local/bin\" >> /home/ec2-user/setup_node.sh\n";
		userData += "chown ec2-user:ec2-user /home/ec2-user/setup_node.sh\n";
		userData += "chmod +x /home/ec2-user/setup_node.sh\n";
		userData += "/home/ec2-user/setup_node.sh\n";
		userData += "/usr/local/bin/redis-server\n";
		return encodeBase64(userData);
	}
	
	private static String getUserData() {
		String userData = "";
		userData += "#!/bin/bash\n";
		userData += "echo \"#!/bin/bash\nyum -y update\nsudo yum -y install gcc make\ncd /usr/local/src \nsudo wget http://download.redis.io/redis-stable.tar.gz\nsudo tar xvzf redis-stable.tar.gz\nsudo rm -f redis-stable.tar.gz\ncd redis-stable\nsudo make distclean\nsudo make\nsudo yum install -y tcl\nsudo cp src/redis-server /usr/local/bin/\nsudo cp src/redis-cli /usr/local/bin/\nsudo cp src/redis-benchmark /usr/local/bin\" >> /home/ec2-user/setup_node.sh\n";
		userData += "chown ec2-user:ec2-user /home/ec2-user/setup_node.sh\n";
		userData += "chmod +x /home/ec2-user/setup_node.sh\n";
		userData += "/home/ec2-user/setup_node.sh\n";
//		userData += "/usr/local/bin/redis-server\n";
		return userData;
	}

	private static String encodeBase64(String input) {
		String base64UserData = null;
		try {
			base64UserData = new String(Base64.encodeBase64(input.getBytes("UTF-8")), "UTF-8");
		} catch (UnsupportedEncodingException uee) {
			System.out.println(uee.getMessage());
		}
		return base64UserData;
	}
	
	public static void main(String[] args) {
		
		String str = getUserDataNormalNode();
		System.out.println(str);
	}
}
