package aws_ec2.tasks;

import java.util.Scanner;

import com.amazonaws.services.ec2.model.Instance;

import aws_ec2.utils.EC2Manager;
import aws_ec2.utils.Key_GroupManager;
import aws_ec2.utils.MyInstanceState;
import aws_ec2.utils.SSHConnection;

public class Homework09 {
	
   	private static String keypairName = "keyPairTesting";
   	private static String securityGroupName = "securityGroupTesting";
   	private static String inputVM = "t2.micro";
   	private static String keypairFilePath = "src/main/resources/keyPairTesting.pem";
   	private static String publicIP;
   	private static String privateIP;
   	
   	public static void main(String[] args) {
		
		EC2Manager ec2 = new EC2Manager();
		initRedisEC2Instance(ec2, keypairName, "sg-0c45a45577cf91df2");
	}

	public static String initRedisEC2Instance(EC2Manager ec2, String keypairName, String secgrID) {

        Instance instance = ec2.requestRunInstance( keypairName, secgrID, inputVM); 
        String instanceID = instance.getInstanceId();
        ec2.setInstanceTag(instance);
        ec2.startInstance( instance);
        
        ec2.waitForInstanceStatus(MyInstanceState.running);
        
        publicIP = ec2.getPublicIP();
        
        SSHConnection con0 = waitForSession(ec2);
        
        String str = getUserData();
        con0.sshCommand(str);

//        solution to start redis-server and terminate java program using screen -d -m
        con0.sshCommand("screen -S session -d -m redis-server\n");
        con0.sshCommand("redis-cli ping\n");
        
        str = getRedisConfData();
        con0.sshCommand(str);
        con0.sshCommand("cat redis.conf");
        
//      solution to start redis-server and terminate java program using screen -d -m
        con0.sshCommand("screen -S session -d -m redis-server redis.conf\n");
        con0.sshCommand("redis-cli -p 7000 ping");
        con0.disconnetSession();
        
        return instanceID;
	}
	
	public static String initSimpleEC2Instance(EC2Manager ec2, String keypairName, String secgrID) {

		Instance instance = ec2.requestRunInstance(keypairName, secgrID, inputVM);
		String instanceID = instance.getInstanceId();
		ec2.setInstanceTag(instance);
		ec2.startInstance(instance);
		
		ec2.waitForInstanceStatus(MyInstanceState.running);

		publicIP = ec2.getPublicIP();

		SSHConnection con0 = waitForSession(ec2);
//		String str = getRedisConfData();
//		con0.sshCommand(str);
//		con0.sshCommand("cat redis.conf");
		con0.sshCommand("echo ciao\n");
		con0.disconnetSession();

		return instanceID;
	}

	
	private static SSHConnection waitForSession(EC2Manager ec2) {
		SSHConnection connection = new SSHConnection(ec2.getPublicIP(), keypairFilePath);
        System.out.println("trying to get connected via ssh");
        while(connection.getSession() == null) {
        	connection.connectSession();
        	try {
        		Thread.sleep(2000);
				System.out.print(".");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		return connection;
		
	}
	
	private static String getUserData() {
		String userData = "";
		userData += "#!/bin/bash\n";
		userData += "echo \"#!/bin/bash\nyum -y update\nsudo yum -y install gcc make\ncd /usr/local/src \nsudo wget http://download.redis.io/redis-stable.tar.gz\nsudo tar xvzf redis-stable.tar.gz\nsudo rm -f redis-stable.tar.gz\ncd redis-stable\nsudo make distclean\nsudo make\nsudo yum install -y tcl\nsudo cp src/redis-server /usr/local/bin/\nsudo cp src/redis-cli /usr/local/bin/\nsudo cp src/redis-benchmark /usr/local/bin\" >> /home/ec2-user/setup_node.sh\n";
		userData += "chown ec2-user:ec2-user /home/ec2-user/setup_node.sh\n";
		userData += "chmod +x /home/ec2-user/setup_node.sh\n";
		userData += "/home/ec2-user/setup_node.sh\n";
		return userData;
	}
	
	private static String getRedisConfData() {
		String data = "";
		data +="echo \"port 7000\n" + 
				"cluster-enabled yes\n" + 
				"bind 0.0.0.0\n" +
				"protected-mode no\n" +
				"cluster-config-file nodes.conf\n" + 
				"cluster-node-timeout 5000\n" + 
				"appendonly yes\" >> /home/ec2-user/redis.conf\n";
		return data;
	}
	
	public static void waitForUserInput() {
		
	    Scanner myObj = new Scanner(System.in);  // Create a Scanner object
	    System.out.println("Enter key to terminate ec2 instance and clean everything");

	    String userName = myObj.nextLine();  // Read user input
	    System.out.println("Entered: " + userName);  // Output user input
	}

}

