package aws_ec2.tasks;

//import org.apache.log4j.Logger;

import com.amazonaws.services.ec2.model.Instance;

import aws_ec2.utils.EC2Manager;
import aws_ec2.utils.Measure;
import aws_ec2.utils.MyInstanceState;
import aws_ec2.utils.SSHConnection;

public class Homework01 {
	
   	private static String keypairName = "keyPairTesting";
   	private static String securityGroupName = "securityGroupTesting";
   	private static String inputVM = "t2.micro";
   	private static String keypairFilePath = "src/main/resources/keyPairTesting.pem";
   	
   	public static void create_terminateInstance() {
   		
        EC2Manager ec2 = new EC2Manager();

        ec2.deleteKeyPair(keypairName);
        ec2.createKeyPair(keypairName);
                
        String secgrID = ec2.createSecurityGroup(securityGroupName);
        System.out.println("secgrID: "+secgrID);

        Measure measure1 = new Measure(inputVM, "starting");
        Instance instance = ec2.requestRunInstance( keypairName, secgrID, inputVM); 
        String instanceID = instance.getInstanceId();
        ec2.setInstanceTag(instance);
        ec2.startInstance( instance);
        
        ec2.waitForInstanceStatus(MyInstanceState.running);
        measure1.stopAndSave();
        
        SSHConnection con0 = waitForSession(ec2);
   		con0.sshCommand("echo hello! && pwd && exit");
//   		connection.sshCommand("pwd");
//        connection.sshCommand("sudo yum install java-1.8.0 -y");
//        connection.sshCommand("java -version");
        con0.disconnetSession();
        
        Measure measure2 = new Measure(inputVM, "terminating");
        ec2.terminateInstance(instanceID);
        ec2.waitForInstanceStatus(MyInstanceState.terminated);
        measure2.stopAndSave();

        ec2.deleteKeyPair(keypairName); 
        ec2.deleteKeyPairPemFile(keypairName);
        ec2.deleteSecurityGroup(secgrID);
        
        System.out.println("Test terminated");
   	}
   	
	private static SSHConnection waitForSession(EC2Manager ec2) {
		SSHConnection connection = new SSHConnection(ec2.getPublicIP(), keypairFilePath);
        System.out.println("trying to get connected via ssh");
        while(connection.getSession() == null) {
        	connection.connectSession();
        	try {
        		Thread.sleep(1000);
				System.out.print(".");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		return connection;
		
	}
   	
}
