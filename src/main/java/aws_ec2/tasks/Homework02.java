package aws_ec2.tasks;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import com.amazonaws.services.ec2.model.Instance;

import aws_ec2.utils.DockerConnection;
import aws_ec2.utils.EC2Manager;
import aws_ec2.utils.Measure;
import aws_ec2.utils.MyInstanceState;
import aws_ec2.utils.SSHConnection;

public class Homework02 {
	
   	private static String keypairName = "keyPairTesting";
   	private static String securityGroupName = "securityGroupTesting";
   	private static String vmType = "t2.micro";
   	private static String keypairFilePath = "src/main/resources/keyPairTesting.pem";
   	private static String measureFilePath = "measure.txt";
   	private static String receivedFilePath = "src/main/resources/Homework02/output.txt";

	public static void task1() {
		
        printMeasureFileHeading(measureFilePath, "EU_CENTRAL_1");
		
		Measure measureOverallTask = new Measure(vmType, "overall task 1");

//	• create a Docker image of calc_fib.jar (the application from Homework 01)
		
		Measure measureDocker = new Measure(vmType, "connect to Docker Hub and push image");

		DockerConnection dockerConn = 
				new DockerConnection("ru34n0c1!", "leoruba", "/home/marco/.docker/config.json", "/home/marco/.docker");	
		dockerConn.buildImage("src/main/resources/Homework02/Dockerfile", "leoruba/homework02:0");
//	• push the created image to Docker Hub.
		dockerConn.pushImage("leoruba/homework02", "0");
		
		measureDocker.stopAndSave();
		
//	• start an EC2 VM instance
		
		Measure measureInstanceCreation = new Measure("t2.micro", "create new ec2 instance and run it");
        
        EC2Manager ec2 = new EC2Manager();

        ec2.deleteKeyPair(keypairName);
        ec2.createKeyPair(keypairName);
                
        String secgrID = ec2.createSecurityGroup(securityGroupName);
        System.out.println("secgrID: "+secgrID);

        Instance instance = ec2.requestRunInstance( keypairName, secgrID, vmType); 
        String instanceID = instance.getInstanceId();
        ec2.setInstanceTag(instance);
        ec2.startInstance( instance);
        
        ec2.waitForInstanceStatus(MyInstanceState.running);
        
        measureInstanceCreation.stopAndSave();

//	• install and start Docker in the started EC2 VM instance
        SSHConnection con0 = waitForSession(ec2);
        con0.sshCommand("sudo yum update -y");
        con0.sshCommand("sudo amazon-linux-extras install docker -y");
        con0.sshCommand("sudo service docker start");
        con0.sshCommand("sudo usermod -a -G docker ec2-user && exit");
        con0.sshCommand("docker info");
        con0.disconnetSession();

//	• select the image from Docker Hub and pull it in the Docker (in the started EC2 VM instance)
//  • run the Docker image
        SSHConnection con1 = waitForSession(ec2);
        con1.sshCommand("docker pull leoruba/homework02:0");
        con1.sshCommand("docker images");        
        con1.sshCommand("docker run leoruba/homework02:0 | tee output.txt");       
        con1.sshCommand("docker ps -a && docker rm $(docker ps -a -q)");
        con1.sshCommand("ls -l && less output.txt");
        
//  • downloading the result to your machine    
		con1.receiveFile("output.txt", receivedFilePath);
        con1.disconnetSession();

        
//	• terminate the EC2 VM instance after the application finishes
        
        Measure measureTerminatingInstanceTime = new Measure(vmType, "terminate ec2 instance and delete key pair and security group");
        
        ec2.terminateInstance(instanceID);
        ec2.waitForInstanceStatus(MyInstanceState.terminated);

        ec2.deleteKeyPair(keypairName); 
        ec2.deleteKeyPairPemFile(keypairName);
        ec2.deleteSecurityGroup(secgrID);
        
        measureTerminatingInstanceTime.stopAndSave();
        
        System.out.println("Test terminated");
        
        measureOverallTask.stopAndSave();
//	• measure the start time and compare it with the time from Homework 1. What do you observe?
		
	}

	private static SSHConnection waitForSession(EC2Manager ec2) {
		SSHConnection connection = new SSHConnection(ec2.getPublicIP(), keypairFilePath);
        System.out.println("trying to get connected via ssh");
        while(connection.getSession() == null) {
        	connection.connectSession();
        	try {
        		Thread.sleep(1000);
				System.out.print(".");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
		return connection;
	}
	
	public void task3() {
		
//		• get id of started docker images
//		• get logs from a container
//		• inspect a container by id to get additional information (e.g. start time)
		
	}
	
    public static void printMeasureFileHeading(String measureFilePath, String region) {
    	
    		Date date = new Date();
//    		String region = ec2.getRegion();
    	
        try {
            FileWriter writer = new FileWriter(measureFilePath, true);

            writer.append(String.format("\n---------- VM type: %s, date and time: %s, zone: %s ---------- \n\n", vmType, date, region ));
            System.out.println(String.format("\n---------- VM type: %s, date and time: %s, zone: %s ---------- \n", vmType, date, region));
            writer.flush();
            writer.close();
        } catch (IOException e) {
//            log.error(e.getMessage());
        	e.printStackTrace();
        }
    }
	
}
