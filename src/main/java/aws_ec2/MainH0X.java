package aws_ec2;

import aws_ec2.tasks.Homework01;
import aws_ec2.tasks.Homework09;
import aws_ec2.utils.EC2Manager;

public class MainH0X {
	
   	private static String keypairName = "keyPairTesting";
   	private static String securityGroupName = "securityGroupTesting";

	public static void main(String[] args) {
		
		EC2Manager ec2_0 = new EC2Manager();
		EC2Manager ec2_1 = new EC2Manager();
		
//		Homework01.create_terminateInstance();
		Homework09.initRedisEC2Instance(ec2_0, keypairName, "sg-01730a14b3eaa6f26");
		Homework09.initRedisEC2Instance(ec2_1, keypairName, "sg-01730a14b3eaa6f26");

		
	}
}
