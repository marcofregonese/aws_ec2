package aws_ec2.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class SSHConnection {

    private static final String username = "ec2-user";
    private static final int port = 22;
    private Session session = null;
    private EC2Manager ec2;
    private static String keypairFilePath;
    private static String publicDNS;
    private static String publicIP;
    private static String vmType = "t2.micro";
    
    public SSHConnection(String publicIP, String keypairFilePath) {
    	
    	this.keypairFilePath = keypairFilePath;
    	this.publicIP = publicIP;
    }

    public SSHConnection(EC2Manager ec2, String keypairFilePath) {
		
		this.ec2 = ec2;
		this.keypairFilePath = keypairFilePath;
		this.publicDNS = ec2.getPublicDNS();
		this.publicIP = ec2.getPublicIP();
	}
    

	public Session getSession() {
		return session;
	}


	public boolean connectSession() {
    	
    	JSch jsch = new JSch();
        try {
            
        	session = jsch.getSession(username, publicIP, port);
            session.setConfig(
                    "PreferredAuthentications",
                    "publickey,gssapi-with-mic,keyboard-interactive,password");
            jsch.addIdentity(keypairFilePath);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();

        } catch (Exception e) {
            e.printStackTrace();
            session = null;
            return false;
        }
        
        System.out.println("connection to "+ publicIP +" active");
        return true;
    }
	
	public void disconnetSession() {
		
		if(session != null) {
			session.disconnect();
			session = null;
			System.out.println("jsch session disconnected");
		}else { System.out.println("session already disconnected");}
			
	}
	
	public boolean sshCommand(String command) {

		Measure m = new Measure(vmType, command);
		if (session != null) {

			Channel channel;
			try {
				channel = session.openChannel("exec");

				((ChannelExec) channel).setCommand(command);
				channel.setInputStream(null);
				((ChannelExec) channel).setErrStream(System.err);

				InputStream input = channel.getInputStream();
				channel.connect();

				System.out.println("command sent: " + command);

				try {
					InputStreamReader inputReader = new InputStreamReader(input);
					BufferedReader bufferedReader = new BufferedReader(inputReader);
					String line = null;

					while ((line = bufferedReader.readLine()) != null) {
						System.out.println(line);
					}
					bufferedReader.close();
					inputReader.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				channel.disconnect();
			} catch (JSchException | IOException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("session not connected");
			return false;
		}
		m.stopAndSave();
		return true;
	}
	
	   public void receiveFile(String remoteFilePath, String receivedFilePath){
	    	
			Measure m = new Measure(vmType, "downloading file");
		   
	        ChannelSftp channel = null;
	        try {
	            channel = (ChannelSftp)session.openChannel("sftp");
	            channel.connect();
	            //If you want you can change the directory using the following line.
	            byte[] buffer = new byte[1024];
	            BufferedInputStream bis = new BufferedInputStream(channel.get(remoteFilePath));
	            File newFile = new File(receivedFilePath);
	            OutputStream os = new FileOutputStream(newFile);
	            BufferedOutputStream bos = new BufferedOutputStream(os);
	            int readCount;
	            while( (readCount = bis.read(buffer)) > 0) {
	                bos.write(buffer, 0, readCount);
	            }
	            bis.close();
	            bos.close();
	        } catch (IOException | SftpException | JSchException e) {
	            System.out.println(e.getMessage());
	        } finally

	        {
	        	m.stopAndSave();	
	            channel.disconnect();
	        }
	    }
	
}
