package aws_ec2.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.List;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairResult;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.DeleteKeyPairRequest;
import com.amazonaws.services.ec2.model.DeleteKeyPairResult;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupRequest;
import com.amazonaws.services.ec2.model.DeleteSecurityGroupResult;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeKeyPairsResult;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsRequest;
import com.amazonaws.services.ec2.model.DescribeSecurityGroupsResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.IpRange;
import com.amazonaws.services.ec2.model.KeyPairInfo;
import com.amazonaws.services.ec2.model.Reservation;

public class Key_GroupManager {
	
    private final String IMAGE_ID = "ami-00aa4671cbf840d82";

    private static String instanceId;
    private static String privateKey;
    private String publicIP;
    private String publicDNS;
    private static String keyPairName;
	private static AmazonEC2 ec2Client = AmazonEC2ClientBuilder.standard()
            							.withRegion(Regions.EU_CENTRAL_1)
            							.build();
	private Regions region = Regions.EU_CENTRAL_1;


	public static void createKeyPair(String keyName) {
		
        CreateKeyPairRequest request = new CreateKeyPairRequest().withKeyName(keyName);
        try {
            CreateKeyPairResult result = ec2Client.createKeyPair(request);
            com.amazonaws.services.ec2.model.KeyPair keyPair = result.getKeyPair();
            privateKey = keyPair.getKeyMaterial();
            keyPairName = keyName;

            // Save private Key
            saveKey(keyName + ".pem", privateKey);

//            log.info(String.format("Key %s created", keyName));
        } catch (AmazonEC2Exception e) {
            if (e.getErrorCode().equals("InvalidKeyPair.Duplicate")) {
//                log.info("Key pair exists");
                keyPairName = keyName;
                return;
            }
//            log.error(e.getMessage(), e);
        }
    }
	
    private static void saveKey(String path, String content) {
        File keyFile = new File(path);
        FileWriter fw;
        try {
            fw = new FileWriter("src/main/resources/" + keyFile);
            fw.write(content);
            fw.close();
        } catch (IOException e) {
        	System.out.println(e);
//            log.error(e.getMessage(), e);
        }
    }
    
	public static boolean isKeyPairAvailable( String keypairName) {
		
		boolean available = false;
		DescribeKeyPairsResult response = ec2Client.describeKeyPairs();
		
        for(KeyPairInfo key_pair : response.getKeyPairs()) {
        	if(key_pair.getKeyName().equals(keypairName)) {
        		System.out.println("Key pair with name " +key_pair.getKeyName()+" exists already");
        		available = true;
        	}
        }
        return available;
	}
	
    public static String getKeyPairFilePath() {
        if (keyPairName == null) {
            getKeyPairName();
        }

        return keyPairName + ".pem";
    }
    
    public static String getKeyPairName() {
        if (keyPairName != null) {
            return keyPairName;
        }

        DescribeInstancesRequest request = new DescribeInstancesRequest();
        request.withInstanceIds(instanceId);
        DescribeInstancesResult describeInstancesRequest = ec2Client.describeInstances(request);
        List<Reservation> reservations = describeInstancesRequest.getReservations();
        for (Reservation reservation : reservations) {
            for (Instance instance : reservation.getInstances()) {
                if (instance.getInstanceId().equals(instanceId))
                    keyPairName =  instance.getKeyName();
                return keyPairName;
            }
        }
        return null;
    }

	public static void deleteKeyPair(String pk) {
		
        DeleteKeyPairRequest kprequest = new DeleteKeyPairRequest().withKeyName(pk);
        DeleteKeyPairResult response = ec2Client.deleteKeyPair(kprequest);

        System.out.println("response from deleting key pair with name "+ pk+": "+ response);
	}
	
	public static boolean deleteKeyPairPemFile(String keypairName) {
        
		boolean ret = false;
		
            try{ 
                ret = Files.deleteIfExists(Paths.get("src/main/resources/"+keypairName+".pem")); 
            } 
            catch(NoSuchFileException e) { 
                System.out.println("No such file/directory exists"); 
            } 
            catch(DirectoryNotEmptyException e) { 
                System.out.println("Directory is not empty."); 
            } 
            catch(IOException e) { 
                System.out.println("Invalid permissions."); 
            } 
            if(ret) {
            	System.out.println("Local "+keypairName+".pem file deleted");
            }else {
            	System.out.println("No local "+keypairName+".pem file founded.");
            }
            return ret;
        
	}
	
	public static String createSecurityGroup(String securityGroupName) {

		CreateSecurityGroupResult createSecurityGroupResult;
		String scID = null;
		try {
			CreateSecurityGroupRequest csgr = new CreateSecurityGroupRequest();
			csgr.withGroupName(securityGroupName).withDescription("Test security group creation");
			createSecurityGroupResult = ec2Client.createSecurityGroup(csgr);
			scID = createSecurityGroupResult.getGroupId();
		} catch (AmazonServiceException e) {
			e.printStackTrace();
			scID = getSecurityGroupId(securityGroupName);
			return scID;
		}
    	//authorize security group ingress
        
    	IpRange ip_range = new IpRange()
    		    .withCidrIp("0.0.0.0/0");

    		IpPermission ip_perm2 = new IpPermission()
    		    .withIpProtocol("tcp")
    		    .withToPort(22)
    		    .withFromPort(22)
    		    .withIpv4Ranges(ip_range);
    		
    		AuthorizeSecurityGroupIngressRequest authorizeSecurityGroupIngressRequest =
    			    new AuthorizeSecurityGroupIngressRequest();

    			authorizeSecurityGroupIngressRequest.withGroupName(securityGroupName)
    			                                    .withIpPermissions(ip_perm2);
    			
    	ec2Client.authorizeSecurityGroupIngress(authorizeSecurityGroupIngressRequest);
    	
		return scID;
	}
	
	public static String createSecurityGroup(String securityGroupName, int tcp_port1, int tcp_port2) {

		CreateSecurityGroupResult createSecurityGroupResult;
		String scID = null;
		try {
			CreateSecurityGroupRequest csgr = new CreateSecurityGroupRequest();
			csgr.withGroupName(securityGroupName).withDescription("Test security group creation");
			createSecurityGroupResult = ec2Client.createSecurityGroup(csgr);
			scID = createSecurityGroupResult.getGroupId();
		} catch (AmazonServiceException e) {
			e.printStackTrace();
			scID = getSecurityGroupId(securityGroupName);
			return scID;
		}
    	//authorize security group ingress
        
    	IpRange ip_range = new IpRange()
    		    .withCidrIp("0.0.0.0/0");

    		IpPermission ip_perm0 = new IpPermission()
    		    .withIpProtocol("tcp")
    		    .withToPort(22)
    		    .withFromPort(22)
    		    .withIpv4Ranges(ip_range);
    		
    		IpPermission ip_perm1 = new IpPermission()
        		    .withIpProtocol("tcp")
        		    .withToPort(tcp_port1)
        		    .withFromPort(tcp_port1)
        		    .withIpv4Ranges(ip_range);
    		
    		IpPermission ip_perm2 = new IpPermission()
        		    .withIpProtocol("tcp")
        		    .withToPort(tcp_port2)
        		    .withFromPort(tcp_port2)
        		    .withIpv4Ranges(ip_range);
    		
    		AuthorizeSecurityGroupIngressRequest authorizeSecurityGroupIngressRequest =
    			    new AuthorizeSecurityGroupIngressRequest();

    			authorizeSecurityGroupIngressRequest.withGroupName(securityGroupName)
    			                                    .withIpPermissions(ip_perm0)
    			                                    .withIpPermissions(ip_perm1)
    			                                    .withIpPermissions(ip_perm2);
    			
    	ec2Client.authorizeSecurityGroupIngress(authorizeSecurityGroupIngressRequest);
    	
		return scID;
	}
	
	public static String getSecurityGroupId( String securityGroupName) {
		
		String scID = "";
		
        DescribeSecurityGroupsRequest request =
                new DescribeSecurityGroupsRequest().withGroupNames(securityGroupName);

        DescribeSecurityGroupsResult response =
                ec2Client.describeSecurityGroups(request);
        
        for(com.amazonaws.services.ec2.model.SecurityGroup group : response.getSecurityGroups()) {
        	if(group.getGroupName().equals(securityGroupName)) {
                scID = group.getGroupId();
        	}
        }
		return scID;
	}
	
	public static void deleteSecurityGroup(String sgID) {
		
		
    	DeleteSecurityGroupRequest sgrequest = new DeleteSecurityGroupRequest().withGroupId(sgID);
    	DeleteSecurityGroupResult sgresponse = ec2Client.deleteSecurityGroup(sgrequest);

    	System.out.println("Successfully deleted security group with id"+ sgID);
    
}
}
