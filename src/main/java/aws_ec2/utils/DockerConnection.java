package aws_ec2.utils;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.command.BuildImageResultCallback;
import com.github.dockerjava.core.command.PushImageResultCallback;

public class DockerConnection {
	
	private DockerClient dockerClient;
	
	//local connection
	public DockerConnection() {
		
		DefaultDockerClientConfig.Builder config = DefaultDockerClientConfig.createDefaultConfigBuilder();
		dockerClient = DockerClientBuilder
								.getInstance(config)
								.build();
		System.out.println("local docker connection established");
	}
	
	//remote connection
	public DockerConnection(String passwd, String username, String pathconfig, String certpath) {
		
		DefaultDockerClientConfig config
		  = DefaultDockerClientConfig.createDefaultConfigBuilder()
		    .withRegistryEmail("leonardorubanoci@gmail.com")
		    .withRegistryPassword(passwd)
		    .withRegistryUsername(username)
		    .withDockerCertPath(certpath)
		    .withDockerConfig(pathconfig)
		    .withDockerTlsVerify("1")
//		    .withDockerHost("unix://var/run/docker.sock")
		    .build();
		 
		dockerClient = DockerClientBuilder.getInstance(config).build();
		System.out.println("connection to Docker Hub established");
	}
	
	public void pushImage(String imageName, String tag) {
		
		try {
			dockerClient.pushImageCmd(imageName)
//				.withAuthConfig(dockerClient.authConfig())
			  .withTag(tag)
			  .exec(new PushImageResultCallback())
			  .awaitCompletion(90, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.format("Pushed image %s to Docker Hub\n", imageName);
	}
	
	public void listActiveContainers() {

		List<Container> containers = dockerClient.listContainersCmd().exec();
		System.out.println("containers: "+ containers.toString());
	}
	
	public void listExitedContainers() {

		List<String> states = Arrays.asList(new String[]{"exited"});
		List<Container> containers = dockerClient.listContainersCmd().withShowAll(true).withStatusFilter(states).exec();
		System.out.println("containers: "+ containers.toString());
	}
	
	public void listImages() {
		
		List<Image> images = dockerClient.listImagesCmd().exec();
		System.out.println("images: "+ images.toString());
	}
	
	public void buildImage(String pathToDockerfile, String nameAndtag) {
		
		String imageId = dockerClient.buildImageCmd()
				  .withDockerfile(new File(pathToDockerfile))
				  .withPull(true)
				  .withNoCache(true)
				  .withTags(new HashSet<String>( Arrays.asList( nameAndtag )))
				  .exec(new BuildImageResultCallback())
				  .awaitImageId();

		System.out.println("image "+ imageId + " " + nameAndtag+ " built");
	}
	
	public void removeLocalImage(String imageId) {
		
		dockerClient.removeImageCmd(imageId).exec();
	}
	
}
