package aws_ec2.utils;

public enum MyInstanceState {

    pending(0),
    running(16),
    shutting_down(32),
    terminated(48),
    stopping(64),
    stopped(80);


    private final int code;

    MyInstanceState(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}

